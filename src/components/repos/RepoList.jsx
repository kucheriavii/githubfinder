import React from 'react'
import { PropTypes } from 'prop-types';
import RepoItem from './RepoItem';

export default function RepoList({repos}) {
  return (
    <div className="rounded-lg shadiw-lg card bg-base-100">
        <div className="card-body">
            <h2 className="text-3xl my-4 font-bold card-title">
                Latest Repositories
            </h2>
            {repos && repos.map((repo) => (
                //<a href={repo.clone_url} className='card rounded-lg' target="_blank" key={repo.id}> {repo.name}</a>
                <RepoItem key={repo.id} repo={repo}/>
            ))}
        </div>
    </div>
  )
}
