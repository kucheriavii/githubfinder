import React from 'react'
import { useState, useContext } from 'react';
import AlertContext from '../../context/alert/AlertConext';
import GithubContext from '../../context/github/GithubContext';

export default function UserSearch() {

    const [text, setText] = useState('')

    const {users, searchUsers, clearUsers} = useContext(GithubContext)
    const {setAlert} = useContext(AlertContext)
    
    const handleChange = (e) => setText(e.target.value)

    const handleSubmit = (e) => {
        e.preventDefault()

        if(text === '') {
            setAlert("Please enter sometthing", "error")
        } else {
            //@todo - search users
            searchUsers(text)
            setText('')
        }
    }

    return (
        <div className='grid grid-cols-1 xl:grid-col-2 ld:grid-cols-2 md:grid-cols-2 mb-8 gap-8'>
            <div>
                <form onSubmit={handleSubmit}>
                    <div className="form-contro">
                        <div className="relative">
                            <input  type="text" 
                                    className="w-full pr-40 bg-gray-200 input input-lg text-black" 
                                    placeholder='Search'
                                    value={text}
                                    onChange={handleChange}/>
                            <button type='submit'
                                    className="absolute top-0 right-0 rounded-l-none w-36 btn btn-lg">Go</button>
                        </div>
                    </div>
                </form>
            </div>
            {users.length > 0 && 
            <div>
                <button onClick={clearUsers} className="btn btn-ghost btn-lg">
                    Clear
                </button>
            </div>}
            
        </div>
    )
}
